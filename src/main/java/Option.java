import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.sql.*;
import java.util.Scanner;

public class Option {
    static  int pageP=1;
    static int size = 3;
    static Scanner input = new Scanner(System.in);
    //color
    static final String ANSI_CYAN = "\u001B[36m";
    static final String ANSI_YELLOW = "\u001B[33m";
    static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    //Function Insert
    public static void write(Connection connection) {
        String insertQuery = "INSERT INTO unsave_product VALUES(default, ?, ?, ?, ?)";
        Product p = Main.getInputFromUser();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, p.getName());
            preparedStatement.setFloat(2, p.getPrice());
            preparedStatement.setInt(3, p.getQty());
            preparedStatement.setString(4, p.getDate());
            int row = preparedStatement.executeUpdate();
            if (row > 0) {
                System.out.println(ANSI_YELLOW+"Insert has been save to the temporary list choose \"Save\" Option to Database!"+ANSI_RESET);
            } else System.out.println("Failed");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static String validateNumber(){
        while (true){
            String validateNumber = new Scanner(System.in).nextLine();
            if (validateNumber.matches("^[0-9]\\d*$")){
                return validateNumber;
            }else {
                System.out.print(ANSI_RED+"Invalid! Please Input Only Number!  :"+ANSI_RESET);
            }
        }
    }
    static Product inputID(){
        Scanner src = new Scanner(System.in);
        System.out.print("Input id to show product : ");
        int id = Integer.parseInt(validateNumber());
        return new Product (id);
    }

    //Function Read
    public static void read(Connection connection){
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 1; Table t = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        t.setColumnWidth(0, 30,50);
        t.addCell(ANSI_CYAN+"Product"+ANSI_RESET,numberStyle);
        String selectQuery = "SELECT * FROM product where id=?";
        Product p = Option.inputID();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
            preparedStatement.setInt(1,p.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float price = resultSet.getFloat("price");
                int qty = resultSet.getInt("qty");
                String date = resultSet.getString("date");
                t.addCell("ID: "+id);
                t.addCell("Name: "+name);
                t.addCell("Price: "+price);
                t.addCell("Qty:  "+qty);
                t.addCell("Import date: "+date);
            }else {
                t.addCell(ANSI_RED+"Product Not Found"+ANSI_RESET,numberStyle);
            }
            System.out.println(t.render());
            System.out.println();
        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
    public static void update(){
        System.out.println("update");
    }
    //Function delete
    public static void delete(){
        System.out.println("delete");
    }
    //Function search
    static Product SearchRow() {
        String name;
        System.out.print("Enter Search name : ");
        name = Main.Input();
        return new Product(name);
    }

    public static int i;
    public static void update(Connection connection) {
        String insertQuery = "INSERT INTO unsave_update VALUES(?, ?, ?, ?, ?)";
        Product p = Main.getUpdateFromUser();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setInt(1, p.getId());
            preparedStatement.setString(2, p.getName());
            preparedStatement.setFloat(3, p.getPrice());
            preparedStatement.setInt(4, p.getQty());
            preparedStatement.setString(5, p.getDate());
            int row = preparedStatement.executeUpdate();
            i=p.getId();
            if (row > 0) {
                System.out.println(ANSI_YELLOW+"Update has been save to the temporary list choose \"Save\" Option to Database!"+ANSI_RESET);
            } else System.out.println("Failed");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    //Function delete
    static Product inputd() {
        System.out.print("Input ID to Delete : ");
        int id = Integer.parseInt(validateNumber());
        return new Product(id);
    }
    public static void delete(Connection connection) {
        Scanner scr = new Scanner(System.in);
        Product product = inputd();
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 1;
        Table t = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        t.setColumnWidth(0, 30, 50);
        t.addCell("product", numberStyle);

        try {
            String deletequery1 = "SELECT* FROM product WHERE id = ?";
            PreparedStatement preparedStatement1 = connection.prepareStatement(deletequery1);
            preparedStatement1.setInt(1, product.getId());
            ResultSet resultSet1 = preparedStatement1.executeQuery();
            if (resultSet1.next()) {
                int id = resultSet1.getInt("id");
                String name = resultSet1.getString("name");
                float price = resultSet1.getFloat("price");
                int qty = resultSet1.getInt("qty");
                String date = resultSet1.getString("date");
                t.addCell("ID : " + id);
                t.addCell("Name : " + name);
                t.addCell("Price : " + price);
                t.addCell("Qty : " + qty);
                t.addCell("Import Date : " + date);
            } else {
                t.addCell(ANSI_RED+"Product Not Found"+ANSI_RESET, numberStyle);
            }
            System.out.println(t.render());
            System.out.println();

            if (t.render().length() != 164) {
                System.out.print(ANSI_YELLOW+"Inter \"y\" to confirm \"n\" to cancel : "+ANSI_RESET);
                String str = scr.nextLine();
                switch (str) {
                    case "y":
                        String deletequery = "DELETE FROM product WHERE id=? ";
                        PreparedStatement preparedStatement = connection.prepareStatement(deletequery);
                        preparedStatement.setInt(1, product.getId());
                        preparedStatement.execute();
                        System.out.println(ANSI_GREEN+"Deleted successfully"+ANSI_RESET);
                        break;
                    case "n":
                        return;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Function search
    public static void search(Connection connection) {
        size = ReadAndWrite.readRow();
        int totalPage=  (int)Math.ceil((float)countP(connection)/size);
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5;
        Table t = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t.setColumnWidth(0, 15, 50);
        t.setColumnWidth(1, 20, 30);
        t.setColumnWidth(2, 20, 30);
        t.setColumnWidth(3, 10, 15);
        t.setColumnWidth(4, 20, 25);
        t.addCell(ANSI_CYAN+"product"+ANSI_RESET, numberStyle, 5);
        t.addCell(ANSI_YELLOW+"ID"+ANSI_RESET, numberStyle);
        t.addCell(ANSI_YELLOW+"Name"+ANSI_RESET, numberStyle);
        t.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET, numberStyle);
        t.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET, numberStyle);
        t.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET, numberStyle);

        try {
            String querysearch = "SELECT * FROM product WHERE name LIKE ?";
            Product pro = SearchRow();
            PreparedStatement preparedStatement = connection.prepareStatement(querysearch);
            preparedStatement.setString(1, "%" + pro.getName()+ "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                t.addCell(String.valueOf(resultSet.getInt("id")), numberStyle);
                t.addCell(resultSet.getString("name"), numberStyle);
                t.addCell(String.valueOf(resultSet.getFloat("price")), numberStyle);
                t.addCell(String.valueOf(resultSet.getInt("qty")), numberStyle);
                t.addCell(resultSet.getString("date"), numberStyle);
                while (resultSet.next()){
                    t.addCell(String.valueOf(resultSet.getInt("id")), numberStyle);
                    t.addCell(resultSet.getString("name"), numberStyle);
                    t.addCell(String.valueOf(resultSet.getFloat("price")), numberStyle);
                    t.addCell(String.valueOf(resultSet.getInt("qty")), numberStyle);
                    t.addCell(resultSet.getString("date"), numberStyle);
                }

            } else {
                t.addCell(ANSI_RED+"Product Not Found!!!"+ANSI_RESET, numberStyle, 5);
            }
            t.addCell(pageP+ANSI_YELLOW+totalPage+ANSI_RESET, numberStyle, 2);
            t.addCell(ANSI_YELLOW+"Total Record"+ANSI_RESET+countP(connection), numberStyle, 3);
            System.out.println(t.render());
            System.out.println();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Function first
    public static void first(Connection connection){
        size = ReadAndWrite.readRow();
        int totalPage=  (int)Math.ceil((float)countP(connection)/size);
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5; Table t1 = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t1.setColumnWidth(0, 10,15);
        t1.setColumnWidth(1,20,30);
        t1.setColumnWidth(2,20,30);
        t1.setColumnWidth(3,10,15);
        t1.setColumnWidth(4,20,25);
        t1.addCell(ANSI_CYAN+"List Product"+ANSI_RESET,numberStyle,5);
        t1.addCell(ANSI_YELLOW+"ID"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Name"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET,numberStyle);
        String selectQuery = "SELECT * FROM product LIMIT " +size;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet=statement.executeQuery(selectQuery);
            pageP=1;

            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float price = resultSet.getFloat("price");
                int qty =  resultSet.getInt("qty");
                String date = resultSet.getString("date");
                t1.addCell(String.valueOf(id));
                t1.addCell(name);
                t1.addCell(String.valueOf(price));
                t1.addCell(String.valueOf(qty));
                t1.addCell(date);

            }
            t1.addCell(pageP+ANSI_YELLOW+" page of "+ANSI_RESET+totalPage,numberStyle,2);
            t1.addCell(ANSI_YELLOW+"Total Record "+ANSI_RESET+countP(connection),numberStyle,3);
            System.out.println(t1.render());
            System.out.println();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Function previous
    static int offsetP(){
        return 3*(pageP-1);
    }
    static int countP (Connection connection){
        String count = "SELECT COUNT(*) FROM product";
        int numberOfRow=0;
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(count);
            ResultSet resultSet= preparedStatement.executeQuery();

            while (resultSet.next()){
                numberOfRow = resultSet.getInt(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return numberOfRow;
    }
    public static void previous(Connection connection){
        size = ReadAndWrite.readRow();
        int totalPage=  (int)Math.ceil((float)countP(connection)/size);
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5; Table t3 = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t3.setColumnWidth(0, 10,15);
        t3.setColumnWidth(1,20,30);
        t3.setColumnWidth(2,20,30);
        t3.setColumnWidth(3,10,15);
        t3.setColumnWidth(4,20,25);
        t3.addCell(ANSI_CYAN+"List Product"+ANSI_RESET,numberStyle,5);
        t3.addCell(ANSI_YELLOW+"ID"+ANSI_RESET,numberStyle);
        t3.addCell(ANSI_YELLOW+"Name"+ANSI_RESET,numberStyle);
        t3.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET,numberStyle);
        t3.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET,numberStyle);
        t3.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET,numberStyle);
        try {
            Statement statement = connection.createStatement();

            pageP--;

            if (pageP<=1){
                pageP=1;
            }
            String selectQuery = "SELECT * FROM product LIMIT "+size+" OFFSET " + offsetP();
            ResultSet resultSet=statement.executeQuery(selectQuery);

            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float price = resultSet.getFloat("price");
                int qty =  resultSet.getInt("qty");
                String date = resultSet.getString("date");
                t3.addCell(String.valueOf(id));
                t3.addCell(name);
                t3.addCell(String.valueOf(price));
                t3.addCell(String.valueOf(qty));
                t3.addCell(date);

            }
            t3.addCell(pageP+" page of "+totalPage,numberStyle,2);
            t3.addCell("Total Record "+countP(connection),numberStyle,3);
            System.out.println(t3.render());
            System.out.println();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    //Function next
    public static void next(Connection connection){
        size = ReadAndWrite.readRow();
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5; Table t2 = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t2.setColumnWidth(0, 10,15);
        t2.setColumnWidth(1,20,30);
        t2.setColumnWidth(2,20,30);
        t2.setColumnWidth(3,10,15);
        t2.setColumnWidth(4,20,25);
        t2.addCell(ANSI_CYAN+"List Product"+ANSI_RESET,numberStyle,5);
        t2.addCell(ANSI_YELLOW+"ID"+ANSI_RESET,numberStyle);
        t2.addCell(ANSI_YELLOW+"Name"+ANSI_RESET,numberStyle);
        t2.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET,numberStyle);
        t2.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET,numberStyle);
        t2.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET,numberStyle);
        try {
            Statement statement = connection.createStatement();
            int totalPage=  (int)Math.ceil((float)countP(connection)/size);
            if (pageP<totalPage) {
                pageP++;
            }
            else{
                pageP=totalPage;
            }String selectQuery = "SELECT * FROM product LIMIT "+size+" OFFSET " + offsetP();
            ResultSet resultSet=statement.executeQuery(selectQuery);
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float price = resultSet.getFloat("price");
                int qty =  resultSet.getInt("qty");
                String date = resultSet.getString("date");
                t2.addCell(String.valueOf(id));
                t2.addCell(name);
                t2.addCell(String.valueOf(price));
                t2.addCell(String.valueOf(qty));
                t2.addCell(date);
            }
            t2.addCell(pageP+ANSI_YELLOW+" page of "+ANSI_RESET+totalPage,numberStyle,2);
            t2.addCell(ANSI_YELLOW+"Total Record "+ANSI_RESET+countP(connection),numberStyle,3);
            System.out.println(t2.render());
            System.out.println();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Function last
    public static void last(Connection connection){
        size = ReadAndWrite.readRow();
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5; Table t4 = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t4.setColumnWidth(0, 10,15);
        t4.setColumnWidth(1,20,30);
        t4.setColumnWidth(2,20,30);
        t4.setColumnWidth(3,10,15);
        t4.setColumnWidth(4,20,25);
        t4.addCell(ANSI_CYAN+"List Product"+ANSI_RESET,numberStyle,5);
        t4.addCell(ANSI_YELLOW+"ID"+ANSI_RESET,numberStyle);
        t4.addCell(ANSI_YELLOW+"Name"+ANSI_RESET,numberStyle);
        t4.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET,numberStyle);
        t4.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET,numberStyle);
        t4.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET,numberStyle);
        try {
            Statement statement = connection.createStatement();
            int totalPage=  (int)Math.ceil((float)countP(connection)/3);
            pageP=totalPage;
            String selectQuery = "SELECT * FROM product LIMIT "+size+" OFFSET " + offsetP();
            ResultSet resultSet=statement.executeQuery(selectQuery);

            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float price = resultSet.getFloat("price");
                int qty =  resultSet.getInt("qty");
                String date = resultSet.getString("date");
                t4.addCell(String.valueOf(id));
                t4.addCell(name);
                t4.addCell(String.valueOf(price));
                t4.addCell(String.valueOf(qty));
                t4.addCell(date);

            }
            t4.addCell(pageP+ANSI_YELLOW+" page of "+ANSI_RESET+totalPage,numberStyle,2);
            t4.addCell(ANSI_YELLOW+"Total Record "+ANSI_RESET+countP(connection),numberStyle,3);
            System.out.println(t4.render());
            System.out.println();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Function go
    public static void go(Connection connection){
        size = ReadAndWrite.readRow();
        int totalPage=  (int)Math.ceil((float)countP(connection)/size);
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5; Table t5 = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t5.setColumnWidth(0, 10,15);
        t5.setColumnWidth(1,20,30);
        t5.setColumnWidth(2,20,30);
        t5.setColumnWidth(3,10,15);
        t5.setColumnWidth(4,20,25);
        t5.addCell(ANSI_CYAN+"List Product"+ANSI_RESET,numberStyle,5);
        t5.addCell(ANSI_YELLOW+"ID"+ANSI_RESET,numberStyle);
        t5.addCell(ANSI_YELLOW+"Name"+ANSI_RESET,numberStyle);
        t5.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET,numberStyle);
        t5.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET,numberStyle);
        t5.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET,numberStyle);
        Scanner src = new Scanner(System.in);
        System.out.print("Enter page: ");
        int page = Integer.parseInt(validateNumber());
        pageP=page;
        System.out.println(pageP);
        String selectQuery = "SELECT * FROM product LIMIT "+size+" OFFSET " + offsetP();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float price = resultSet.getFloat("price");
                int qty = resultSet.getInt("qty");
                String date = resultSet.getString("date");
                t5.addCell(String.valueOf(id));
                t5.addCell(name);
                t5.addCell(String.valueOf(price));
                t5.addCell(String.valueOf(qty));
                t5.addCell(date);
                while (resultSet.next()){
                    id = resultSet.getInt("id");
                    name = resultSet.getString("name");
                    price = resultSet.getFloat("price");
                    qty = resultSet.getInt("qty");
                    date = resultSet.getString("date");
                    t5.addCell(String.valueOf(id));
                    t5.addCell(name);
                    t5.addCell(String.valueOf(price));
                    t5.addCell(String.valueOf(qty));
                    t5.addCell(date);
                }
            }else {
                t5.addCell(ANSI_RED+"Product Not Found"+ANSI_RED,numberStyle,5);
            }
            t5.addCell(pageP+ANSI_YELLOW+" page of "+ANSI_RESET+totalPage,numberStyle,2);
            t5.addCell(ANSI_YELLOW+"Total Record "+ANSI_RESET+countP(connection),numberStyle,3);
            System.out.println(t5.render());
            System.out.println();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    //Function save
    public static void save(Connection connection) {
        try {
            Statement st = connection.createStatement();
            st.executeUpdate("INSERT INTO product(id, name, price, qty, date) SELECT id, name, price, qty, date from unsave_product;");
            st.executeUpdate("truncate unsave_product;");
            st.executeUpdate("update product p set name = pu.name, price = pu.price, qty = pu.qty, date = pu.date from (select * from unsave_update where id = "+i+" ) pu where p.id=pu.id;");
            st.executeUpdate("truncate unsave_update;");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //Function unsave
    public static void unsave(Connection connection) {
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5;
        Table t = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t.setColumnWidth(0, 10, 15);
        t.setColumnWidth(1, 20, 30);
        t.setColumnWidth(2, 20, 30);
        t.setColumnWidth(3, 10, 15);
        t.setColumnWidth(4, 20, 25);

        String i;
        System.out.print(ANSI_YELLOW+"\"I\" for unsave insertion and \"U\" for unsave update : "+ANSI_RESET);
        i = input.nextLine();
        switch (i){
            case "i" :{
                t.addCell(ANSI_CYAN+"Unsave Insert"+ANSI_RESET, numberStyle, 5);
                t.addCell(ANSI_YELLOW+"ID"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Name"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET, numberStyle);
                String selectQuery = "SELECT * FROM unsave_product";
                    try {
                        Statement statement = connection.createStatement();
                        ResultSet resultSet = statement.executeQuery(selectQuery);
                        while (resultSet.next()) {
                            int id = resultSet.getInt("id");
                            String name = resultSet.getString("name");
                            float price = resultSet.getFloat("price");
                            int qty = resultSet.getInt("qty");
                            String date = resultSet.getString("date");
                            t.addCell(String.valueOf(id));
                            t.addCell(name);
                            t.addCell(String.valueOf(price));
                            t.addCell(String.valueOf(qty));
                            t.addCell(date);
                        }
                        if (t.render().length()==512){
                            t.addCell(ANSI_RED+"No Data!"+ANSI_RESET, numberStyle, 5);
                        }
                        System.out.println(t.render());
                        System.out.println();

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                break;
            }
            case "u" :{
                t.addCell(ANSI_CYAN+"Unsave Update"+ANSI_RESET, numberStyle, 5);
                t.addCell(ANSI_YELLOW+"ID"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Name"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET, numberStyle);
                t.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET, numberStyle);
                String selectQuery = "SELECT * FROM unsave_update";

                try {
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery(selectQuery);
                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        String name = resultSet.getString("name");
                        float price = resultSet.getFloat("price");
                        int qty = resultSet.getInt("qty");
                        String date = resultSet.getString("date");
                        t.addCell(String.valueOf(id));
                        t.addCell(name);
                        t.addCell(String.valueOf(price));
                        t.addCell(String.valueOf(qty));
                        t.addCell(date);
                    }
                    if (t.render().length()==512){
                        t.addCell(ANSI_RED+"No Data!"+ANSI_RESET, numberStyle, 5);
                    }
                    System.out.println(t.render());
                    System.out.println();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    //Function backup
    public static void backup(){

        System.out.println("backup");
    }
    //Function restore
    public static void restore(){
        System.out.println("rrrr");
    }

    //Function setrow
    public static void setrow(Connection connection ){
        System.out.print("Enter row: ");
        int row = Integer.parseInt(validateNumber());

        ReadAndWrite.writeRow(row);

        int totalPage=  (int)Math.ceil((float)countP(connection)/row);
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        int totalColumn = 5; Table t1 = new Table(totalColumn, BorderStyle.UNICODE_BOX_DOUBLE_BORDER, ShownBorders.ALL);
        t1.setColumnWidth(0, 10,15);
        t1.setColumnWidth(1,20,30);
        t1.setColumnWidth(2,20,30);
        t1.setColumnWidth(3,10,15);
        t1.setColumnWidth(4,20,25);
        t1.addCell(ANSI_YELLOW+"List Product"+ANSI_RESET,numberStyle,5);
        t1.addCell(ANSI_YELLOW+"ID"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Name"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Unit_price"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Qty"+ANSI_RESET,numberStyle);
        t1.addCell(ANSI_YELLOW+"Import Date"+ANSI_RESET,numberStyle);
        String setRowQuery = "SELECT * FROM product LIMIT ? OFFSET " + offsetP();
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(setRowQuery);
            preparedStatement.setInt(1,row);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                t1.addCell(String.valueOf(resultSet.getInt("id")));
                t1.addCell(resultSet.getString("name"));
                t1.addCell(String.valueOf(resultSet.getFloat("price")));
                t1.addCell(String.valueOf(resultSet.getInt("qty")));
                t1.addCell(resultSet.getString("date"));
            }
            t1.addCell(pageP+ANSI_YELLOW+" page of "+ANSI_RESET+totalPage,numberStyle,2);
            t1.addCell(ANSI_YELLOW+"Total Record "+ANSI_RESET+countP(connection),numberStyle,3);
            System.out.println(t1.render());
            System.out.println();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}






