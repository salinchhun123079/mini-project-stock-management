import java.sql.*;
import java.util.Scanner;


public class Main {
    static Scanner scanner = new Scanner(System.in);
    public static final String ANSI_RED = "\u001B[31m";
    public static final String RESET = "\033[0m";
    static long millis=System.currentTimeMillis();

    //string validation
    static String Input(){
        while (true){
            String Input = new Scanner(System.in).nextLine();
            if (Input.matches("[a-zA-Z\\s]+")){
                return Input;
            }else {
                System.out.print(ANSI_RED+"Please input only text! :"+ANSI_RED);
            }
        }
    }
    static java.sql.Date date = new java.sql.Date(millis);
    static Product getInputFromUser(){
        System.out.print("Enter name : ");
        String name = scanner.nextLine();

        float price = 0;
        boolean isNumber;
        do {
            System.out.print("Enter Price : ");
            if(scanner.hasNextInt()){
                price = scanner.nextFloat();
                isNumber = true;
            }else{;
                System.out.println(ANSI_RED + "Your price must be contains with the number!!!"+RESET);
                isNumber = false;
                scanner.next();
            }
        }while (!(isNumber));

        int qty = 0;
        do {
            System.out.print("Update Qty : ");
            if(scanner.hasNextInt()){
                qty = scanner.nextInt();
                isNumber = true;
            }else{
                System.out.println(ANSI_RED + "Your qty must be contains with the number!!!"+RESET);
                isNumber = false;
                scanner.next();
            }
        }while (!(isNumber));
        System.out.println(date);
        return new Product(name ,price, qty, date);
    }
    static Product getUpdateFromUser() {
        boolean isNumber;
        int id = 0;
        do {
            System.out.print("Enter ID to update : ");
            if(scanner.hasNextInt()){
                id = scanner.nextInt();
                isNumber = true;
            }else{
                System.out.println(ANSI_RED + "Your ID must be contains with the number!!!"+RESET);
                isNumber = false;
                scanner.next();
            }
        }while (!(isNumber));
        System.out.print("Update Name : ");
        scanner.nextLine();
        String name = scanner.nextLine();
        float price = 0;
        do {
            System.out.print("Enter Price : ");
            if(scanner.hasNextInt()){
                price = scanner.nextFloat();
                isNumber = true;
            }else{
                System.out.println(ANSI_RED + "Your price must be contains with the number!!!"+RESET);
                isNumber = false;
                scanner.next();
            }
        }while (!(isNumber));

        int qty = 0;
        do {
            System.out.print("Update Qty : ");
            if(scanner.hasNextInt()){
                qty = scanner.nextInt();
                isNumber = true;
            }else{
                System.out.println(ANSI_RED + "Your qty must be contains with the number!!!"+RESET);
                isNumber = false;
                scanner.next();
            }
        }while (!(isNumber));

        System.out.println(date);
        return new Product(id, name, price, qty, date);
    }
    public static void write(Connection connection){
        System.out.println("Write");
    }

    public static void main(String[] args) throws SQLException {
        ConnectionDB connectionDB = new ConnectionDB();
        Connection connection = connectionDB.getConnection();

        Scanner input = new Scanner(System.in);
        String option="";
        Option.first(connection);
        do {
            System.out.println("*)Display");
            System.out.println("|| W).Write\t\t| R).Read\t\t\t| U).Update\t\t\t| D).Delete\t\t\t| S).Search   ||");
            System.out.println("|| F).First\t\t| P).Previous\t\t| N).Next\t\t\t| L).Last\t\t\t| G).Goto     ||");
            System.out.println("|| Sa).Save\t\t| Un).Unsave\t\t| Ba).Backup\t\t| Re).Restore\t\t| Se).Set Row ||");
            System.out.println("|| E).Exit");
            System.out.print("Enter option : ");
            option = Input().toLowerCase();
            switch (option){
                case "w":{
                    Option.write(connection);
                    break;
                }
                case "r":{
                    Option.read(connection);
                    break;
                }
                case "u":{
                    Option.update(connection);
                    break;
                }
                case "d":{
                    Option.delete(connection);
                    break;
                }
                case "s":{
                    Option.search(connection);
                    break;
                }
                case "f":{
                    Option.first(connection);
                    break;
                }
                case "p":{
                    Option.previous(connection);
                    break;
                }
                case "n":{
                    Option.next(connection);
                    break;
                }
                case "l":{
                    Option.last(connection);
                    break;
                }
                case "g":{
                    Option.go(connection);
                    break;
                }
                case "sa":{
                    Option.save(connection);
                    break;
                }
                case "un":{
                    Option.unsave(connection);
                    break;
                }
                case "ba":{
                    Option.backup();
                    break;
                }
                case "re":{
                    Option.restore();
                    break;
                }
                case "se":{
                    Option.setrow(connection);
                    break;
                }
                case "e":{
                    System.exit(0);
                    break;
                }
            }
        }while (option!="");
    }
}