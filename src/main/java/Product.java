import java.sql.Date;
import java.time.LocalDate;

public class Product {
    private int id;
    private String name;
    private float price;
    private int qty;
    String date = String.valueOf(LocalDate.now());

    public Product(int id){
        this.id = id;
    }

    public Product(String name) {
        this.name = name;
    }

    public Product(int id, String name, float price, int qty, Date date) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.date = String.valueOf(date);
    }

    public Product(String name, float price, int qty, Date date) {
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.date= String.valueOf(date);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

