import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ReadAndWrite {
    static void writeRow(int row){
        try {
            FileWriter fWriter = new FileWriter(
                    "row.txt");
            // Writing into file
            // Note: The content taken above inside the
            // string
            fWriter.write(String.valueOf(row));

            // Closing the file writing connection
            fWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    static int readRow(){
        int row= 3;
        try {
            File myObj = new File("row.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                row= Integer.parseInt(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return row;
    }

}

