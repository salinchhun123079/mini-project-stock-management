import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB{
        java.sql.Connection getConnection(){
            java.sql.Connection connection = null;
            try{
                connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/stock_management", "postgres", "123079");
            }catch (SQLException e){
                e.printStackTrace();
            }
            return connection;
        }
}